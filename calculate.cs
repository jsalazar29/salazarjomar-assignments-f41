﻿using System;

namespace calculate
{

    //Basic Calculation interface
    interface IBasicCalculation{
        void BasiCalculationMethod();
    }

    //Basic Calculation Class
    class BasicCalc : IBasicCalculation{
        public void BasiCalculationMethod(){
            Console.WriteLine("Basic Calculation");
            string firstNumber;
            string secondNumber;
            string calOperation;

            Console.Write("\nEnter First Number\n");
            firstNumber = Console.ReadLine();
            double fNum = Convert.ToDouble(firstNumber);

            Console.Write("\nEnter Second Number\n");
            secondNumber = Console.ReadLine();
            double sNum = Convert.ToDouble(secondNumber);

            Console.Write("Choose Operation: 1(Addition), 2(Subtraction), 3(Multiplication), 4(Division)");
            calOperation = Console.ReadLine();
            int basicOperation = Convert.ToInt32(calOperation);

            switch (basicOperation){
                case 1:
                    Console.WriteLine("\nAnswer is : {0} \n", fNum + sNum); 
                    break;

                case 2:
                    Console.WriteLine("\nAnswer is : {0} \n", fNum - sNum); 
                    break;
                
                case 3:
                    Console.WriteLine("\nAnswer is : {0} \n", fNum * sNum);     
                    break;
                case 4:
                    Console.WriteLine("\nAnswer is : {0} \n", fNum / sNum);     
                    break;
            }
        }
    }

    //Scintific Calculations Interface
    interface IScientificCalculation{
    void ScientificCalculationMethod();
    }

    //Scientific Calculation Class
    class SciCal : IScientificCalculation{
        public void ScientificCalculationMethod(){
            Console.WriteLine("Basic Calculation");
            string firstNumber;
            string sciOperation;

            Console.Write("\nEnter Number\n");
            firstNumber = Console.ReadLine();
            double fNum = Convert.ToDouble(firstNumber);

            Console.Write("\nChoose Operation: 1(Tangent), 2(Cosine), 3(Sine) \n");
            sciOperation = Console.ReadLine();
            int Op = Convert.ToInt32(sciOperation);

            switch (Op){
                case 1:
                    double tan = (fNum * (Math.PI)) / 180;
                    Console.WriteLine(Math.Tan(tan));
                    break;

                case 2:
                    double cos = (fNum * (Math.PI)) / 180;
                    Console.WriteLine(Math.Cos(cos));
                    break;
                
                case 3:
                    double sin = (fNum * (Math.PI)) / 180;
                    Console.WriteLine(Math.Tan(sin));
                    break;
            }
        }
    }
    interface IBinaryConversion{
        void BinaryConversionMethod();
    }

    class BinaryConversion : IBinaryConversion{
        public void BinaryConversionMethod(){
            int  n, i;       
            int[] a = new int[10];
            Console.Write("Enter the number to convert: ");    
            n= int.Parse(Console.ReadLine());     
            for(i=0; n>0; i++)      
                {      
                a[i]=n%2;      
                n= n/2;    
                }      
            Console.Write("Binary of the given number= ");      
            for(i=i-1 ;i>=0 ;i--)      
            {      
                Console.Write(a[i]);      
            }         

                }
    }
    class Program
    {
        static void Main(string[] args)
        {
            BasicCalc calc = new BasicCalc();
            SciCal sc = new SciCal();
            BinaryConversion binary = new BinaryConversion();

            calc.BasiCalculationMethod();
            sc.ScientificCalculationMethod();
            binary.BinaryConversionMethod();
        }
    }
}           

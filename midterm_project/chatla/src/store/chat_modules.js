import { userServices } from '../services/user-services';

const user = JSON.parse(localStorage.getItem('user'));
const state = {
    user: user,
    allUsers: {},
    messages: {},
    convo: {},
    otherUser: null
};

const actions = {
    getAll({ commit }) {
        commit('getAllRequest');
        userServices.getAll()
            .then(
                allusers => commit('getAllSuccess', allusers),
                error => commit('getAllFailure', error)
            );
    },

    getConvo({ commit }) {
        commit('getAllRequest');
        userServices.getConvo()
            .then(
                convo => commit('getAllConvo', convo),
                error => commit('getAllFailure', error)
            );
    },
    getMessages({commit}, otherUserId){
        console.log(otherUserId);
        
        userServices.getMessages(otherUserId)
            .then(
                messages => {
                    commit('getSuccess', messages)
                })
    }, 

    sendMessages({commit}, {message, reciever, sender}){
        userServices.sendMessages(message, reciever, sender)
            .then(
                usermessage=> {
                    setTimeout(() => {console.log("Message Sent") })
                    commit('sendSuccess', usermessage)
                },  
                error => {
                    console.log(error);
                }
            );
    },

    getOtherUser({commit}, otherUserId){
        userServices.getOtherUser(otherUserId)
            .then(
                getOtherUser => commit('getOtherUserSuccess', getOtherUser),
                error => console.log(error)
            )
    }
};

const mutations = {
    messageNotSent(state, error){
        state.messages = { error };
    },
    getSuccess(state, messages){
        state.messages = { messages };
    },
    registerSuccess(state, message) {
        state.messages = {};
    },
    sendSuccess(state, usermessage){
    
    },
    messageRequest(state){
         state.messages = { loading: true };
    },
    messageNotSent(message, error) {
        state.status = {};
    },
    getAllRequest(state) {
        state.allUsers = { loading: true };
    },
    getAllSuccess(state, allusers) {
        state.allUsers = {allusers}
    },
    getAllConvo(state, convo) {
        state.convo = {convo}
    },
    getAllFailure(state, error) {
        state.allUsers = { error };
    },
    getOtherUserSuccess(state, getOtherUser){
        state.otherUser = getOtherUser
    }
};
const getters = {
    users: state => {
        return state.allUsers
    }
}

export const users = {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
};

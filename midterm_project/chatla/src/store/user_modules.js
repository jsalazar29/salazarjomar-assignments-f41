import { userServices } from '../services/user-services';
import { createRouter } from '../routes';
import chathub from '../chathub.js'

const user = JSON.parse(localStorage.getItem('user'));
const state =  user
    ? { status: { loggedIn: true }, user }
    : { status: {}, user: null };

const actions = {
    login({ dispatch, commit }, user) {
        commit('loginRequest', user);
    
        userServices.login(user)
            .then(
                user => {
                    commit('loginSuccess', user);
                    createRouter.push('/chats')
                },
                error => {
                    commit('loginFailure', error);
                    dispatch('alert/error', error, { root: true });
                }
            );
    },

    logout({ commit }) {
        userServices.logout();
        commit('logout');
    },
    register({ dispatch, commit }, user) {
        console.log(user);
        commit('registerRequest', user);
    
        userServices.register(user)
        .then(
            user => {
                commit('registerSuccess', user);
                setTimeout(() => {
                    // display success message after route change completes
                    dispatch('alert/success', 'Registration successful', { root: true });
                })
            },
            error => {
                commit('registerFailure', error);
                dispatch('alert/error', error, { root: true });
            }
        );
    }
};

const mutations = {
    loginRequest(state, user) {
        state.status = { loggedIn: true };
        state.user = user;
    },
    loginSuccess(state, user) {
        state.status = { loggedIn: true };
        state.user = user;
    },
    loginFailure(state) {
        state.status = {};
        state.user = null;
    },
    logout(state) {
        state.status = {};
        state.user = null;
    },
    registerRequest(state, user) {
        state.status = { registering: true };
    },
    registerSuccess(state, user) {
        state.status = {};
    },
    registerFailure(state, error) {
        state.status = {};
    }
};

export const account = {
    namespaced: true,
    state,
    actions,
    mutations
};
using System;
using server.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using server.Helpers;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace server.Services
{
    public interface IUserServices
    {
        UserModel Authenticate(string email, string password);
        UserModel Register(UserModel user);
    }

    public class UserServices : IUserServices
    {
        private DataContext _context;
        private readonly AppSettings _appSettings;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public UserServices(DataContext context, IOptions<AppSettings> appSettings, IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _appSettings = appSettings.Value;
            _context = context;
        }
        public UserModel Authenticate(string email, string password)
        {   
            var user = _context.Users.SingleOrDefault(x => x.Email == email && x.Password == password);

            if (user == null)
                return null;
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.key);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new System.Security.Claims.ClaimsIdentity(new Claim[]{
                    
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            user.Password = null;
            return user;
        }
        public UserModel Register(UserModel user)
        {
            if (_context.Users.Any(x => x.Email == user.Email))
                throw new AppException("Username \"" + user.Email + "\" is already taken");

            _context.Users.Add(user);
            _context.SaveChanges();

            return user;
        }
        
    }
}
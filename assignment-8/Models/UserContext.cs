﻿using Microsoft.EntityFrameworkCore;

namespace ass7.Models
{
    public class UserContext : DbContext
    {
        public UserContext()
        {
        }

        public UserContext(DbContextOptions<UserContext> options) : base(options)
        {

        }

        public DbSet<UserProfileModel> UserProfile {get; set; }
    }
}
